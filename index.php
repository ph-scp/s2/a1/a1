<?php require_once "./code.php"

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>

  <style>

  </style>
</head>

<body>
  <div class="containerOne">
    <h1>Divisible of Five</h1>
    <p><?= getDivisibleByFive(); ?></p>
  </div>

  <div class="containerOne">
    <h1>Array Manipulation</h1>
    <p><?= print_r(getStudentName("John Smith 1")); ?></p>
    <p><?= print_r(getStudentName("John Smith 2")); ?></p>
    <p><?= print_r(getStudentName("John Smith 3")); ?></p>
  </div>
</body>

</html>